require_relative "../../lib/controller"
class MainController < Controller
  def index
    @arr = %w(one two three)
  end
  def hello
    @test = "Some dump text here"
  end
end